# Movie Tickets
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
## Features:
- Filter for better search movie and ticket, comment on the movie detail page
- Login, register, and book tickets
- Admin features: CRUD users, movies
- Responsive mobile, tablet and desktop
- Implement a Progressive Web App with Create React App
## Technologies:
React Hooks, Material-UI, Redux, Redux-thunk, Axios, Bootstrap 4(Css)
- Username: admin2023, password: 123456789
## Link Deploy:
http://hideous-letters.surge.sh/